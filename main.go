package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
)

func PingHandler(c *fiber.Ctx) error {
	return c.SendString("Pong!")
}

func HelloWorldHandler(c *fiber.Ctx) error {
	return c.SendString("Hello world :)")
}

func main() {
	app := fiber.New()

	app.Get("/", HelloWorldHandler)

	app.Get("/ping", PingHandler)

	log.Fatal(app.Listen(":3000"))

}
